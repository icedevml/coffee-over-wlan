/**

Coffee Over WLAN Firmware
File: hardware.c
Author: Michal Leszczynski, Marek Klimowicz

**/
#include <avr/io.h>
#include "hardware.h"

unsigned char readPin(unsigned char port, unsigned char pin) {
	port=(port-'A')*3; //+PIN_OFF;
	return (_SFR_IO8(port) & (1 << pin)) == (1 << pin);
}

unsigned char readPort(unsigned char port) {
	port=(port-'A')*3; //+PIN_OFF;
	return _SFR_IO8(port);
}

void setDirection(unsigned char port, unsigned char pin, unsigned char dir) {
	port=(port-'A')*3+DDR_OFF;
	if(dir) {
		_SFR_IO8(port) |= (1 << pin);
	} else {
	   _SFR_IO8(port) &= ~(1 << pin);
	}
}

void writePin(unsigned char port, unsigned char pin, unsigned char val) {
	port=(port-'A')*3+PORT_OFF;
	if(val) {
		_SFR_IO8(port) |= (1 << pin);
	} else {
		_SFR_IO8(port) &= ~(1 << pin);
	}
}

void writePort(unsigned char port, unsigned char val) {
	port=(port-'A')*3+PORT_OFF;
	_SFR_IO8(port) = val;
}

void togglePin(unsigned char port, unsigned char pin) {
	port=(port-'A')*3+PORT_OFF;
	_SFR_IO8(port) ^= (1 << pin);
}

void togglePort(unsigned char port) {
	port=(port-'A')*3+PORT_OFF;
	_SFR_IO8(port) = ~_SFR_IO8(port);
}
