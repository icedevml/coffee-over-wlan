/**

Coffee Over WLAN Firmware
File: networking.c
Author: Michal Leszczynski, Marek Klimowicz

**/
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include "const.h"
#include "uart.h"
#include "main.h"
#include "http.h"
#include "core.h"
#include "networking.h"

unsigned char net_process_state = 0;
char http_method[6];
char http_path[31];
char* http_query;

void net_accept_connection(void) {
	int data = uart1_getc();
	if (!(data & UART_NO_DATA)) {
		if ((char)data == '~') {
			net_process_state = NET_READ_FIRST_LINE;
		}
	}
}

void net_read_first_line(void) {
	if (read_line()) {
		uart_putc('\r');
		uart_putc('\n');
		uart_puts(buf);
		int tmp;
		if (sscanf(buf, "%5s %30s HTTP/%d.%d", http_method, http_path, &tmp, &tmp) != 4) {
			bad_request_response(PSTR("Unknown/too long request line."));
			return;
		}
		
		http_query = strstr_P(http_path, PSTR("?"));
		if (http_query != NULL) {
			*http_query = '\0';
			http_query++;
		}
		net_process_state = NET_READ_HEADERS;
	}
}

void net_read_headers(void) {
	if (read_line()) {
		uart_putc('\r');
		uart_putc('\n');
		uart_puts(buf);
		// if (strstr_P(buf, PSTR("Authorization: Basic ")) == buf && strcmp_P(buf+21, password) == 0) {
			net_process_state = 3;
		/* } else if (*buf == '\0') { // end of input
			send_response(PSTR("401 Authorization Required\r\nWWW-Authenticate: Basic realm=\"Coffee Over WLAN\""), content_text_plain, PSTR("Invalid password."));
		} */
	}
}

void net_make_response(void) {
	if (STR_EQUAL_P(http_path, "/")) {
		redirect_response(PSTR(GUI_ADDRESS));
	// } else if (STR_EQUAL_P(http_path, "/favicon.ico")) {
	// 	redirect_response(PSTR(GUI_ADDRESS "/favicon.ico"));
	} else if (STR_EQUAL_P(http_path, "/status")) {
		status_response();
    } else if (STR_EQUAL_P(http_path, "/power-off")) {
        scheduled_coffee = 0;
        cm_power(0);
    } else if (STR_EQUAL_P(http_path, "/power-on")) {
        cm_power(1);
	} else if (STR_EQUAL_P(http_path, "/coffee-small")) {
		if (scheduled_coffee == 0) {
			scheduled_coffee = 1;
			send_response(status_ok, content_text_javascript, net_response_coffee_accepted);
		} else {
			send_response(status_ok, content_text_javascript, net_response_coffee_busy);
		}
	} else if (STR_EQUAL_P(http_path, "/coffee-big")) {
		if (scheduled_coffee == 0) {
			scheduled_coffee = 2;
			send_response(status_ok, content_text_javascript, net_response_coffee_accepted);
		} else {
			send_response(status_ok, content_text_javascript, net_response_coffee_busy);
		}
	} else if (STR_EQUAL_P(http_path, "/coffee-cancel")) {
		if (scheduled_coffee != 0) {
			scheduled_coffee = 0;
			send_response(status_ok, content_text_javascript, net_response_coffee_accepted);
		} else {
			send_response(status_ok, content_text_javascript, net_response_coffee_invalid);
		}
	} else {
		bad_request_response(PSTR("Unknown path."));
	}
}

void tick_network(void) {
	switch(net_process_state) {
		case NET_ACCEPT_CONNECTION: net_accept_connection(); break;
		case NET_READ_FIRST_LINE: net_read_first_line(); break;
		case NET_READ_HEADERS: net_read_headers(); break;
		case NET_MAKE_RESPONSE: net_make_response(); break;
	}
}
