/**

Coffee Over WLAN Firmware
File: http.c
Author: Michal Leszczynski, Marek Klimowicz

**/
#include <stdio.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include "const.h"
#include "main.h"
#include "coffee.h"
#include "uart.h"
#include "http.h"
#include "networking.h"

char buf[BUF_SIZE]; // http request buffer
char buf2[BUF2_SIZE]; // helper

int pos = 0;

/*
 * Respond with 400 Bad Request and provide a bit of details
 */
void bad_request_response(const char reason[]) {
	send_res_headers(PSTR("400 Bad Request"), content_text_plain, 13+strlen_P(reason));
	uart01_puts_p(PSTR("Bad request! "));
	uart01_puts_p(reason);
}

/*
 * Redirect user using 301 response code (favicon.ico requests, for example)
 */
void redirect_response(const char url[]) {
	net_process_state = 0;

	uart01_puts_p(PSTR("HTTP/1.1 301 Moved Permanently\r\n"));
	uart01_puts_p(PSTR("Location: "));
	uart01_puts_p(url);
	uart01_puts_p(PSTR("\r\n\r\n"));
}

/*
 * Generate dynamic response of coffee machine status
 */
void status_response(void) {
	send_res_headers(status_ok, content_text_javascript, 16);
	sprintf(buf2, "setStatus(%d, %d);", maker_state, scheduled_coffee);
	uart01_puts(buf2);
}

/*
 * Send standard HTTP response *headers*
 */
void send_res_headers(const char status[], const char content_type[], int len) {
	net_process_state = 0;

	uart01_puts_p(PSTR("HTTP/1.1 "));
	uart01_puts_p(status);
	uart01_puts_p(PSTR("\r\nConnection: close\r\nAccess-Control-Allow-Origin: *\r\nCache-Control: no-cache\r\nContent-Type: "));
	uart01_puts_p(content_type);
	uart01_puts_p(PSTR("\r\nContent-Length: "));
	sprintf(buf, "%d", len);
	uart01_puts(buf);
	uart01_puts_p(PSTR("\r\n\r\n"));
}

/*
 * Send complete, standard HTTP response
 */
void send_response(const char status[], const char content_type[], const char body[]) {
	send_res_headers(status, content_type, strlen_P(body));
	uart01_puts_p(body);
}

/*
 * Read line of request from uart1.
 * Returns:
 * * FALSE - operation is still incomplete
 * * FALSE (and sets net_process_state to zero) - buffer overflow or client disconnected
 * * TRUE - complete line has been put in buffer successfuly
 */
int read_line(void) {
	int data = 0;
	while(1) {
		data = uart1_getc();
		if (data & UART_NO_DATA) {
			return 0;
		}
		
		buf[pos] = (char)data;
		if (pos == BUF_SIZE-1) { // looks like buffer overflow
			pos = 0;
			bad_request_response(PSTR("Headers are too large."));
			return 0;
		}
		
		if (buf[pos] == '#') { // client disconnected
			uart_putc('#');
			net_process_state = 0;
			pos = 0;
			return 0;
		}
		
		if (buf[pos] == '\r') { // ignore CR character
			continue;
		}
		
		if (buf[pos] == '\n') { // reached end of line, success
			buf[pos] = '\0';
			pos = 0;
			return 1;
		}
		
		pos++;
	}
}
