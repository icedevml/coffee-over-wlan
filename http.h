/**

Coffee Over WLAN Firmware
File: http.h
Author: Michal Leszczynski, Marek Klimowicz

**/
#pragma once

#define BUF_SIZE 256
#define BUF2_SIZE 10

extern char buf[BUF_SIZE];
extern char buf2[BUF2_SIZE];
extern int pos;

void bad_request_response(const char reason[]);
void redirect_response(const char url[]);
void status_response(void);
void send_res_headers(const char status[], const char content_type[], int len);
void send_response(const char status[], const char content_type[], const char body[]);
int read_line(void);