/**

Coffee Over WLAN Firmware
File: hardware.h
Author: Michal Leszczynski, Marek Klimowicz

**/
#pragma once

#define PIN_OFF 0
#define DDR_OFF 1
#define PORT_OFF 2

#define DIR_INPUT 0
#define DIR_OUTPUT 1

unsigned char readPin(unsigned char port, unsigned char pin);
unsigned char readPort(unsigned char port);
void setDirection(unsigned char port, unsigned char pin, unsigned char dir);
void writePin(unsigned char port, unsigned char pin, unsigned char val);
void writePort(unsigned char port, unsigned char val);
void togglePin(unsigned char port, unsigned char pin);
void togglePort(unsigned char port);