/**

Coffee Over WLAN Firmware
File: core.h
Author: Michal Leszczynski, Marek Klimowicz

**/
#define STR_EQUAL_P(s1, s2)  strcmp_P((s1), PSTR(s2)) == 0
#define STR_STARTS_P(s1, s2)  strstr_P((s1), PSTR(s2)) == (s1)
#define CHAR_BETWEEN(c, x1, x2)  ((c) >= x1 && (c) <= x2)