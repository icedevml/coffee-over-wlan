/**

Coffee Over WLAN Firmware
File: coffee.c
Author: Michal Leszczynski, Marek Klimowicz

**/
#include <stdio.h>
#include <string.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>

#include "uart.h"
#include "coffee.h"
#include "hardware.h"

// led
volatile char prev_led_state = LED_OFF;
volatile char led_state = LED_OFF;

// coffee maker
volatile char prevmaker_state = CM_OFF;
volatile char maker_state = CM_OFF;

// counters
volatile unsigned char rtc_ticks = 0;
volatile unsigned char led_ticks = 0;
volatile unsigned char last_led_delay = 0;

// RTC
ISR(TIMER2_OVF_vect) {
	rtc_ticks++;
	led_ticks++;

	// update coffee maker state
	if (rtc_ticks >= 18) {
		rtc_ticks = 0;
		prevmaker_state = maker_state;

		if (led_state == LED_OFF && led_ticks >= 18) {
			maker_state = CM_OFF;
			led_ticks = 18; // overflow prevention
		} else if (led_state == LED_ON && led_ticks >= 18) {
			maker_state = CM_READY;
			led_ticks = 18;
		} else if (last_led_delay >= 15 && last_led_delay <= 17) {
			maker_state = CM_BUSY;
		} else if (last_led_delay <= 3) {
			maker_state = CM_ERROR;
		} else {
			maker_state = CM_UNKNOWN;
		}
	}
}

// LED
ISR(PCINT2_vect) {
	prev_led_state = led_state;
	led_state = !!readPin('C', 5);
	last_led_delay = led_ticks;
	led_ticks = 0;
}

// Additional PB0
ISR(PCINT1_vect) {
	//uart_putc('~');
}

int cm_power(int mode) {
	if ((mode && maker_state != CM_OFF) || (!mode && (maker_state == CM_OFF || maker_state == CM_UNKNOWN))) {
		return 0;
	}
	
	writePin('C', POWER_PIN, 1);
	_delay_ms(500);
	writePin('C', POWER_PIN, 0);
	return 1;
}

int cm_coffee(int count) {
	if (maker_state != CM_READY) {
		return 0;
	}
	
	char pin = (count == 1 ? C1_PIN : C2_PIN);
	
	writePin('C', pin, 1);
	_delay_ms(500);
	writePin('C', pin, 0);
	return 1;
}
