/**

Coffee Over WLAN Firmware
File: networking.h
Author: Michal Leszczynski, Marek Klimowicz

**/
#pragma once

extern unsigned char net_process_state;
extern char http_method[6];
extern char http_path[31];

enum NET_PROCESS_STATE {
	NET_ACCEPT_CONNECTION = 0,
	NET_READ_FIRST_LINE = 1,
	NET_READ_HEADERS = 2,
	NET_MAKE_RESPONSE = 3
};

void tick_network(void);