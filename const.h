/**

Coffee Over WLAN Firmware
File: const.h
Author: Michal Leszczynski, Marek Klimowicz

**/
#pragma once

#define GUI_ADDRESS "http://192.168.1.106/kawa"

/* progmem strings */
extern const char msg_coffee_ascii[] PROGMEM;
extern const char msg_coffee_ascii2[] PROGMEM;

extern const char msg_before_terminal[] PROGMEM;
extern const char msg_enter_terminal[] PROGMEM;
extern const char msg_exit_terminal[] PROGMEM;
extern const char msg_before_wifly[] PROGMEM;

extern const char status_ok[] PROGMEM;
extern const char content_text_plain[] PROGMEM;
extern const char content_text_html[] PROGMEM;
extern const char content_text_javascript[] PROGMEM;

extern const char net_response_coffee_accepted[] PROGMEM;
extern const char net_response_coffee_busy[] PROGMEM;
extern const char net_response_coffee_invalid[] PROGMEM;

extern const char password[] PROGMEM;