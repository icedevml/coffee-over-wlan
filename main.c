/**

Coffee Over WLAN Firmware
File: main.c
Author: Michal Leszczynski, Marek Klimowicz

**/
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "core.h"
#include "const.h"
#include "main.h"
#include "uart.h"
#include "coffee.h"
#include "http.h"
#include "networking.h"
#include "hardware.h"

// define CPU frequency if it wasn't not defined in Makefile
#ifndef F_CPU
	#define F_CPU 20000000UL
#endif

// applies to both uart0 and uart1
#define UART_BAUD_RATE 115200

// Wifly terminal mode, allows to issue commands to wifly from uart0
void terminal_mode(void) {
	int data = 0;
    while(1) {
        // receive
        data = uart1_getc();
        if (!(data & UART_NO_DATA)) {
            uart_putc((char)data);
        }

        // transmit
        data = uart_getc();
		if ((char)data == '`') {
			uart_puts_p(msg_exit_terminal);
			return;
		}
		
        if (!(data & UART_NO_DATA) && (((char)data >= 32 && (char)data < 127) || (char)data == '\r')) {
            uart1_putc((char)data);
        }
	}
}

// extended COM/USB debug interface
void tick_debug(void) {
	int data = uart_peekc(0);
	if (!(data & UART_NO_DATA)) {
		return;
	}
	
	switch((char)data) {
		case 'T': // toggle
			if (uart_available() >= 2) {
				uart_getc();
				unsigned char port, pin;
				port = (unsigned char)uart_getc();
				pin = (unsigned char)uart_getc();
				togglePin(port, pin-'0');
				uart_putc('!');
			}
			break;
		
		case 'W': // write
			if (uart_available() >= 3) {
				uart_getc();
				unsigned char port, pin, val;
				port = (unsigned char)uart_getc();
				pin = (unsigned char)uart_getc();
				val = (unsigned char)uart_getc();
				writePin(port, pin-'0', val-'0');
				uart_putc('!');
			}
			break;
		
		case 'R': //read
			if (uart_available() >= 2) {
				uart_getc();
				unsigned char port, pin;
				port = (unsigned char)uart_getc();
				pin = (unsigned char)uart_getc();
				uart_putc(readPin(port, pin-'0') ? '1' : '0');
			}
			break;
		
		default:
			uart_getc();
			uart_putc('?');
			break;
	}
}

// listen to commands incoming from wifly
void wifly_mode(void) {
	if (net_process_state == 0) {
		tick_debug();
		tick_hardware();
	}
	tick_network();
}

int scheduled_coffee = 0;

void tick_hardware(void) {
	if (scheduled_coffee > 0) {
		if (maker_state == CM_OFF) { // wake up
			uart_puts_p(PSTR("\r\nWAKEUP\r\n"));
			cm_power(1);
			_delay_ms(750);
		} else if (maker_state == CM_READY) {
			uart_puts_p(PSTR("\r\nCOFFEE <3\r\n"));
			cm_coffee(scheduled_coffee);
			scheduled_coffee = 0;
			_delay_ms(750);
		}
	}
}

void prog_start(void) {
	uart_puts_p(msg_coffee_ascii);
	uart_puts_p(PSTR(__DATE__));
	uart_putc(' ');
	uart_puts_p(PSTR(__TIME__));
	uart_puts_p(msg_coffee_ascii2);
	
	//eeprom_busy_wait();
	uart_puts_p(msg_before_terminal);
	_delay_ms(1000);
	char d = (char)uart_getc();
	if (d == '`') {
		uart_puts_p(msg_enter_terminal);
		terminal_mode();
	}
	
	uart_puts_p(msg_before_wifly);
	while(1) {
		wifly_mode();
	}
}

int main(void)
{
    /*
     *  Initialize UART library, pass baudrate and AVR cpu clock
     *  with the macro
     *  UART_BAUD_SELECT() (normal speed mode )
     *  or
     *  UART_BAUD_SELECT_DOUBLE_SPEED() ( double speed mode)
     */

    DDRC |= 1;

	PCMSK3 |= (1 << 7);
	PCMSK2 |= (1 << 5);
	PCMSK1 |= (1 << 0);

    PCICR |= (1 << 3);
    PCICR |= (1 << 2);
	PCICR |= (1 << 1);

    PORTA = 0;
 	PORTB = 0;
    PORTC = 0;
    PORTD = 0;

    TCCR2B |= (0 << 0);
    TCCR2B |= (1 << 1);
    TCCR2B |= (0 << 2);
    TIMSK2 |= (1 << 0);
    ASSR |= (1 << 5);

	DDRC |= (1 << 4);
	DDRC |= (1 << 3);
	DDRC |= (1 << 2);

    uart_init(UART_BAUD_SELECT(UART_BAUD_RATE, F_CPU));
    uart1_init(UART_BAUD_SELECT(UART_BAUD_RATE, F_CPU));
	
    /*
     * now enable interrupt, since UART library is interrupt controlled
     */

    sei();

    prog_start();

    while(1);
}
