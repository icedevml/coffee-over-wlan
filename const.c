/**

Coffee Over WLAN Firmware
File: const.c
Author: Michal Leszczynski, Marek Klimowicz

**/
#include <avr/pgmspace.h>
#include "const.h"

const char msg_coffee_ascii[] PROGMEM = "\r\n    )))\r\n    (((       Coffee Over WLAN by monq&neonek\r\n  +-----+     Greetz to A. Potocka\r\n  |     |]    Firmware v.1.0 compiled at ";
const char msg_coffee_ascii2[] PROGMEM = "\r\n  `-----'\r\n\r\n";

const char msg_before_terminal[] PROGMEM = "-- Press ` to enter terminal mode...\r\n";
const char msg_enter_terminal[] PROGMEM = "-- Entering terminal mode...\r\n";
const char msg_exit_terminal[] PROGMEM = "-- Exitting terminal mode...\r\n";
const char msg_before_wifly[] PROGMEM = "-- Entering WiFly mode...\r\n";

const char status_ok[] PROGMEM = "200 OK";
const char content_text_plain[] PROGMEM = "text/plain";
const char content_text_html[] PROGMEM = "text/html; charset=utf-8";
const char content_text_javascript[] PROGMEM = "text/javascript";

const char net_response_coffee_accepted[] PROGMEM = "coffeeCommandResponse('accepted');";
const char net_response_coffee_busy[] PROGMEM = "coffeeCommandResponse('busy');";
const char net_response_coffee_invalid[] PROGMEM = "coffeeCommandResponse('invalid');";

const char password[] PROGMEM = "dXNlcjpjb2ZmZWU=";