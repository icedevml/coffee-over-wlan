/**

Coffee Over WLAN Firmware
File: coffee.h
Author: Michal Leszczynski, Marek Klimowicz

**/
#pragma once

#define POWER_PIN 4
#define C2_PIN 3
#define C1_PIN 2

enum ACTION {
	ACTION_PRESS = 0,
	ACTION_RELEASE = 1
};

void cm_button(int pin, int action);
int cm_power(int mode);
int cm_coffee(int count);

enum COFFEE_LED_STATE {
	LED_OFF = 0,
	LED_ON = 1
};

enum COFFEE_MAKER_STATE {
	CM_UNDEFINED = 255,
	CM_OFF = 0,
	CM_ERROR = 1,
	CM_BUSY = 2,
	CM_READY = 3,
	CM_UNKNOWN = 4
};

extern volatile char maker_state;